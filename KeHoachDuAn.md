Kế hoạch hoàn thành dự án qua 6 giao đoạn
* Giai đoạn 1 (Từ 25/10/2017 đến 31/10/2017): Lập nhóm, chọn đề tài, ngôn  ngữ sử dụng HTML và CSS, PHP ,SQL.

* Giai đoạn 2 (Từ 1/11/2017 đến 7/11/2017): Phân tích chức năng của website:

+ Dành cho khách hàng: Đặt tour, tìm kiếm. 

+ Dành cho người quản trị: Thêm, sửa, xóa tour, thống kê doanh thu. Thêm sửa xóa khách sạn nhà hàng .

* Giai đoạn 3 (Từ 8/11/2017 đến 14/11/2017): Thiết kế hệ thống  

- Sơ đồ phân cấp.

- Luồng dữ liệu (Hệ thống đặt tour, quản lý đơn đặt, quản lý tour). 

- Hệ thống cơ sở dữ liệu. 

* Giai đoạn 4 (Từ 15/11/2017 đến 21/11/2017): Demo Website 

- Xây dựng giao diện:

+ Trang chủ 

+ Chi tiết tour

+ Trang Login admin

+ Thêm tour mới 

+ Thống kê 

+ Tìm kiếm

- Xây dựng các chức năng cần thực hiện đúng theo yêu cầu(Phần bắt buộc + Phần tự chọn) của bài tập lớn.

* Giai đoạn 5 (Từ 22/11/2017 đến 28/11/2017): 

- Kiểm tra các chức năng đã đầy đủ đúng theo yêu cầu bài tập lớn không.

- Bổ sung và hoàn thành Website.

* Giai đoạn 6 (Từ 29/11/2017 đến 5/12/2017): Viết báo cáo và soạn slide thuyết trình. 

